<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gallery`.
 */
class m161127_201155_create_gallery_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gallery', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'active' => $this->integer(),
            'sort' => $this->integer(),
            'type_gallery' => $this->integer(),
            'image_title' => $this->string()->notNull(),
            'image_name' => $this->string()->notNull(),
            'image_description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('gallery');
    }
}
