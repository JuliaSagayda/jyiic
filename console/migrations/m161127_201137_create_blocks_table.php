<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blocks`.
 */
class m161127_201137_create_blocks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('blocks', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'active' => $this->integer(),
            'sort_block' => $this->integer(),
            'type_block' => $this->integer(),
            'block_name' => $this->string()->notNull()->unique(),
            'menu_title' => $this->string()->notNull(),
            'image_name' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('blocks');
    }
}
