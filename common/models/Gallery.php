<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $active
 * @property integer $sort
 * @property integer $type_gallery
 * @property string $image_title
 * @property string $image_name
 * @property string $image_description
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'active', 'sort', 'type_gallery'], 'integer'],
            [['image_title', 'image_name'], 'required'],
            [['image_description'], 'string'],
            [['image_title', 'image_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'active' => 'Active',
            'sort' => 'Sort',
            'type_gallery' => 'Type Gallery',
            'image_title' => 'Image Title',
            'image_name' => 'Image Name',
            'image_description' => 'Image Description',
        ];
    }
}
