<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $active
 * @property integer $sort_block
 * @property integer $type_block
 * @property string $block_name
 * @property string $menu_title
 * @property string $image_name
 * @property string $title
 * @property string $description
 */
class Blocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'active', 'sort_block', 'type_block'], 'integer'],
            [['block_name', 'menu_title', 'image_name', 'title'], 'required'],
            [['description'], 'string'],
            [['block_name', 'menu_title', 'image_name', 'title'], 'string', 'max' => 255],
            [['block_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'active' => 'Active',
            'sort_block' => 'Sort Block',
            'type_block' => 'Type Block',
            'block_name' => 'Block Name',
            'menu_title' => 'Menu Title',
            'image_name' => 'Image Name',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }
}
